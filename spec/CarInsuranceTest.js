const expect = require('chai').expect;

const CarInsurance = require('../objects/CarInsurance');
const Product = require('../objects/Product');


describe("CarInsurance", function() {

  it("pass undefined to CarInsurance", function() {
    const coTest = new CarInsurance(undefined);
    expect(coTest.products).to.eql([]);
  });

  it("pass string to CarInsurance", function() {
    const coTest = new CarInsurance('a');
    expect(coTest.products).to.eql([]);
  });

  it("pass number to CarInsurance", function() {
    const coTest = new CarInsurance(1);
    expect(coTest.products).to.eql([]);
  });

  it("pass Product to CarInsurance", function() {
    const coTest = new CarInsurance([new Product('type', 0, 0)]);
    expect(coTest.products[0].name).to.eql("type");
  });

  it("pass Product type Medium Coverage to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Medium Coverage', 10, 20),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Medium Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(19);
  });

  it("pass Product type Medium Coverage with price negative to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Medium Coverage', 10, -10),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Medium Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(0);
  });

  it("pass Product type Medium Coverage with price positive and greater than 50 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Medium Coverage', 10, 55)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Medium Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(50);
  });

  it("pass Product type Medium Coverage with sellIn less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Medium Coverage', -1, 40)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Medium Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(-2);
  expect(carInsurance.products[0].price).to.eql(38);
  });

  it("pass Product type Full Coverage to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Full Coverage', 10, 40)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(41);
  });

  it("pass Product type Full Coverage with price less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Full Coverage', 10, -1)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(0);
  });

  it("pass Product type Full Coverage with price greater than 50 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Full Coverage', 10, 51)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(50);
  });

  it("pass Product type Full Coverage with sellIn less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Full Coverage', -1, 40)
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(-2);
  expect(carInsurance.products[0].price).to.eql(42);
  });

  it("pass Product type Mega Coverage CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Mega Coverage', 0, 80),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Mega Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(0);
  expect(carInsurance.products[0].price).to.eql(80);
  });

  it("pass Product type Special Full Coverage CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', 15, 20),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(14);
  expect(carInsurance.products[0].price).to.eql(21);
  });

  it("pass Product type Special Full Coverage with sellIn less than or equal to 10 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', 10, 20),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(9);
  expect(carInsurance.products[0].price).to.eql(22);
  });

  it("pass Product type Special Full Coverage with sellIn less than or equal to 5 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', 5, 20),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(4);
  expect(carInsurance.products[0].price).to.eql(23);
  });

  it("pass Product type Special Full Coverage with sellIn less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', -1, 20),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(-2);
  expect(carInsurance.products[0].price).to.eql(0);
  });

  it("pass Product type Special Full Coverage with price less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', -1, -1),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(-2);
  expect(carInsurance.products[0].price).to.eql(0);
  });

  it("pass Product type Special Full Coverage with price greater than 50 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Special Full Coverage', -1, 51),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Special Full Coverage");
  expect(carInsurance.products[0].sellIn).to.eql(-2);
  expect(carInsurance.products[0].price).to.eql(50);
  });

  it("pass Product type Super Sale CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Super Sale', 3, 6),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Super Sale");
  expect(carInsurance.products[0].sellIn).to.eql(2);
  expect(carInsurance.products[0].price).to.eql(4);
  });

  it("pass Product type Super Sale with price less than 0 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Super Sale', 3, -1),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Super Sale");
  expect(carInsurance.products[0].sellIn).to.eql(2);
  expect(carInsurance.products[0].price).to.eql(0);
  });

  it("pass Product type Super Sale with price greater than 50 to CarInrusance", function() {
    const productsAtDayZero = [
      //name, sellIn, price
      new Product('Super Sale', 3, 51),
  ];
  
  const carInsurance = new CarInsurance(productsAtDayZero);
  carInsurance.updatePrice();
  expect(carInsurance.products[0].name).to.eql("Super Sale");
  expect(carInsurance.products[0].sellIn).to.eql(2);
  expect(carInsurance.products[0].price).to.eql(50);
  });

});