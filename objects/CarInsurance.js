class CarInsurance {
    constructor(products = []) {
        Array.isArray(products) ? this.products = products : this.products = []
    }
    updatePrice() {
        this.products.forEach(product => {
            switch (product.name) {
                case 'Full Coverage':
                    return product.fullCoverage;
                case 'Special Full Coverage':
                    return product.specialFullCoverage;
                case 'Mega Coverage':
                    return product.megaCoverage;
                case 'Super Sale':
                    return product.superSale;
                default:
                    return product.defaultCoverage;
            }
        });
        return this.products;
    }
}

module.exports = CarInsurance;