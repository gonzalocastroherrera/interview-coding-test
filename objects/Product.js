class Product {
    constructor(name, sellIn, price) {
        this.name = name; //product name
        this.sellIn = sellIn; //number of days we have to sell the product 
        this.price = price; //how much the product cost
    }

    sellIn0() {
        return this.sellIn < 0 ? true : false
    }

    checkPrice() {
        return this.price >= 50 ? true : false
    }

    get fullCoverage() {
        if (this.price >= 0 && this.price < 50) {
            this.sellIn0(this.sellIn) ? this.price += 2 : this.price += 1;
        } else if (this.price < 0) {
            this.price = 0;
        } else if (this.price > 50) {
            this.price = 50;
        }
        this.checkPrice(this.price) ? this.price = 50 : this.price > 0 ? this.price = this.price : this.price = 0;
        this.sellIn -= 1;

        return { name: this.name, sellIn: this.sellIn, price: this.price };
    }

    get defaultCoverage() {
        if (this.price >= 0 && this.price < 50) {
            this.sellIn0(this.sellIn) ? this.price -= 2 : this.price -= 1;
        } else if (this.price < 0) {
            this.price = 0;
        } else if (this.price > 50) {
            this.price = 50;
        }
        this.checkPrice(this.price) ? this.price = 50 : this.price > 0 ? this.price = this.price : this.price = 0;
        this.sellIn -= 1;
        return {
            name: this.name,
            sellIn: this.sellIn,
            price: this.price
        }
    }

    get specialFullCoverage() {
        if (this.price >= 0 && this.price < 50) {
            if (this.sellIn <= 10 && this.sellIn > 5) {
                this.price += 2;
            }
            else if (this.sellIn <= 5 && this.sellIn > 0) {
                this.price += 3;
            } else if (this.sellIn < 0) {
                this.price = 0;
            } else {
                this.price += 1
            }
        } else if (this.price < 0) {
            this.price = 0;
        } else if (this.price > 50) {
            this.price = 50;
        }
        this.checkPrice(this.price) ? this.price = 50 : this.price > 0 ? this.price = this.price : this.price = 0;
        this.sellIn -= 1;

        return {
            name: this.name,
            sellIn: this.sellIn,
            price: this.price
        }
    }

    get megaCoverage() {
        return {
            name: this.name,
            sellIn: this.sellIn,
            price: this.price
        }
    }

    get superSale() {
        if (this.price >= 0 && this.price < 50) {
            this.price -= 2;
        } else if (this.price < 0) {
            this.price = 0;
        } else if (this.price > 50) {
            this.price = 50;
        }
        this.checkPrice(this.price) ? this.price = 50 : this.price > 0 ? this.price = this.price : this.price = 0;
        this.sellIn -= 1;
        return {
            name: this.name,
            sellIn: this.sellIn,
            price: this.price
        }
    }
}

module.exports = Product;
